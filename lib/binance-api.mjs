import Api from 'node-binance-api'

const BinanceApi = superclass => class extends superclass {
  constructor () {
    super()
    this.dataSource = (new Api().options()).websockets
  }
}

export default BinanceApi
