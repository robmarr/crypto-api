import bodyParser from 'body-parser'

import express from 'express'

const AppMixin = superclass => class extends superclass {
  constructor () {
    super()
    const app = this.app = express()
    app.use(bodyParser.urlencoded({ extended: true }))
    app.use(bodyParser.json())
    app.use((error, request, response, next) => {
      response.status(400).send(`Error: ${error}`)
    })
  }
}

export default AppMixin
