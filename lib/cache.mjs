import NodeCache from 'node-cache'

const cache = new NodeCache()
const CacheMixin = superclass => class extends superclass {
  constructor () {
    super()
    this.cache = cache
  }
}

export default CacheMixin
