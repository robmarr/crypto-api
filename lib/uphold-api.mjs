import fetch from 'node-fetch'

const UpholdAPI = superclass => class extends superclass {
  constructor (opts = {}) {
    super(opts)
    this.url = 'https://api.uphold.com/v0'
  }

  fetchTicker (symbol) {
    return fetch(`${this.url}/ticker/${symbol}`)
      .then(response => response.json())
  }
}

export default UpholdAPI
