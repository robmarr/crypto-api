import { join } from 'path'

export default class API {
  constructor (opts = {}) {
    this.apiBase = '/api/1.0'
    this._server = opts.server
    this._cache = opts.cache
  }

  makeAPIPath (...path) {
    return join(this.apiBase, ...path)
  }

  getPrices () {
    const exchanges = this._cache.keys()
    return exchanges.reduce((prices, exchange) => ({
      [exchange]: this._cache.get(exchange),
      ...prices
    }), {})
  }

  getPrice (pair) {
    const exchanges = this._cache.keys()
    return exchanges.reduce((prices, exchange) => ({
      [exchange]: this._cache.get(exchange)[pair],
      ...prices
    }), {})
  }

  getExchangePrices (exchange) {
    return this._cache.get(exchange)
  }

  getExchangePrice (exchange, pair) {
    return this.getExchangePrices(exchange)[pair].toString()
  }

  start () {
    const server = this._server
    server.get(this.makeAPIPath('prices'), (req, res) => (
      res.send(this.getPrices())
    ))

    server.get(this.makeAPIPath('prices', ':pair'), (req, res) => {
      const { pair } = req.params
      res.send(this.getPrice(pair))
    })

    server.get(this.makeAPIPath(':exchange', 'prices'), (req, res) => {
      const { exchange } = req.params
      res.send(this.getExchangePrices(exchange))
    })

    server.get(this.makeAPIPath(':exchange', 'price', ':pair'), (req, res) => {
      const { exchange, pair } = req.params
      res.send(this.getExchangePrice(exchange, pair))
    })
  }
}
