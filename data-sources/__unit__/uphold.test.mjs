import test from 'ava'
import sinon from 'sinon'
import { mockUpholdAPI, tinyTickerResponse } from '../__mocks__/uphold.mock.mjs'
import { UpholdPure } from '../uphold.mjs'

test.before(t => {
  t.context.clock = sinon.useFakeTimers({ toFake: ['setInterval', 'clearInterval'] })
})

test.afterEach.always(t => {
  t.context.clock.restore()
})

test('default cacheKey is uphold', t => {
  const uphold = new UpholdPure()
  t.is(uphold.cacheKey, 'uphold')
})

test('respects cacheKey option', t => {
  const uphold = new UpholdPure({ cacheKey: 'puphold' })
  t.is(uphold.cacheKey, 'puphold')
})

test('non-string cacheKey throws an Error', t => {
  t.throws(() => new UpholdPure({ cacheKey: [] }), { instanceOf: TypeError })
})

test('default interval is 10 minutes in milliseconds', t => {
  const uphold = new UpholdPure()
  t.is(uphold.interval, 600000)
})

test('respects interval option', t => {
  const uphold = new UpholdPure({ interval: 1 })
  t.is(uphold.interval, 60000)
})

test('non-number interval throws an Error', t => {
  t.throws(() => new UpholdPure({ interval: [] }), { instanceOf: TypeError })
})

test('default tickers are GBP, USD, ETH and BTC', t => {
  const uphold = new UpholdPure()
  t.is(uphold.tickers.length, 4)
  // we don't care what order they're in
  t.true(uphold.tickers.includes('USD'))
  t.true(uphold.tickers.includes('BTC'))
  t.true(uphold.tickers.includes('GBP'))
  t.true(uphold.tickers.includes('ETH'))
})

test('respects tickers option', t => {
  const tickers = ['JPY', 'VND', 'BWP', 'LAK']
  const uphold = new UpholdPure({ tickers })
  t.deepEqual(uphold.tickers, tickers)
})

test('non-array tickers throws an Error', t => {
  t.throws(() => new UpholdPure({ tickers: {} }), { instanceOf: TypeError })
})

test('@intervalToMilliseconds converts minutes to milliseconds correctly', t => {
  t.is(UpholdPure.intervalToMilliseconds(0.1), 6000)
  t.is(UpholdPure.intervalToMilliseconds(0.5), 30000)
  t.is(UpholdPure.intervalToMilliseconds(0.75), 45000)
  t.is(UpholdPure.intervalToMilliseconds(1), 60000)
  t.is(UpholdPure.intervalToMilliseconds(1.75), 105000)
})

test('@intervalToMilliseconds throws an Error for invalid arguments', t => {
  t.throws(() => UpholdPure.intervalToMilliseconds('Not a number'), { instanceOf: TypeError }, 'failed for `string`')
  t.throws(() => UpholdPure.intervalToMilliseconds([]), { instanceOf: TypeError }, 'failed for `[]`')
  t.throws(() => UpholdPure.intervalToMilliseconds({}), { instanceOf: TypeError }, 'failed for `{}`')
  t.throws(() => UpholdPure.intervalToMilliseconds(NaN), { instanceOf: TypeError }, 'failed for `NaN`')
  t.throws(() => UpholdPure.intervalToMilliseconds(Infinity), { instanceOf: TypeError }, 'failed for `Infinity`')
})

test('@parseTickerData converts api data to the cache data structure', t => {
  t.deepEqual(UpholdPure.parseTickerData(tinyTickerResponse), { ADAETH: 0.00051371919742575 })
})

test('@parseTickerData throws an Error with invalid input data', t => {
  t.throws(() => UpholdPure.parseTickerData('Not an array'), { instanceOf: TypeError }, 'failed for `string`')
  t.throws(() => UpholdPure.parseTickerData({}), { instanceOf: TypeError }, 'failed for `{}`')
  t.throws(() => UpholdPure.parseTickerData(NaN), { instanceOf: TypeError }, 'failed for `NaN`')
  t.throws(() => UpholdPure.parseTickerData(Infinity), { instanceOf: TypeError }, 'failed for `Infinity`')
})

test('starts the api requests and timer when start is called', t => {
  const Uphold = mockUpholdAPI(UpholdPure)
  const uphold = new Uphold({ tickers: ['BTC'] })
  uphold.start()
  t.true(uphold.fetchTicker.calledOnceWith('BTC'))
  t.truthy(uphold.timer)
  uphold.stop()
})

test('stop clears timers', t => {
  const Uphold = mockUpholdAPI(UpholdPure)
  const uphold = new Uphold()
  uphold.start()
  uphold.stop()
  t.is(uphold.timers, null)
})

test('emits data when received', t => {
  t.plan(2)
  const Uphold = mockUpholdAPI(UpholdPure)
  const uphold = new Uphold()
  const assertions = new Promise(resolve => {
    uphold.on('data', payload => {
      t.is(payload.cacheKey, 'uphold')
      t.like(payload.data, { AEDETH: 1, ETHBTC: 2, ETHUSD: 3, ETHGBP: 4 })
      return resolve()
    })
    uphold.on('error', () => {
      t.fail('this test should not receive an error event')
    })
  }).catch(error => t.fail(error))
  uphold.start()
  return assertions
})

test('emits error when invalid data received', t => {
  t.plan(2)
  const Uphold = mockUpholdAPI(UpholdPure)
  const uphold = new Uphold({ tickers: ['FAIL'] })
  const assertions = new Promise(resolve => {
    uphold.on('data', () => {
      t.fail('this test should not receive a data event')
    })
    uphold.on('error', error => {
      t.true(error instanceof Error)
      t.truthy(error.message)
      resolve()
    })
  }).catch(error => t.fail(error))
  uphold.start()
  return assertions
})

test('emits more data after the interval has passed', t => {
  t.plan(4)
  let emitted = 0
  const Uphold = mockUpholdAPI(UpholdPure)
  const uphold = new Uphold({ interval: 0.01 })
  const assertions = new Promise(resolve => {
    uphold.on('data', payload => {
      emitted++
      t.is(payload.cacheKey, 'uphold')
      if (emitted === 1) {
        t.like(payload.data, { AEDETH: 1, ETHBTC: 2, ETHUSD: 3, ETHGBP: 4 })
      }
      if (emitted === 2) {
        t.like(payload.data, { AEDETH: 1.1, ETHBTC: 2.1, ETHUSD: 3.1, ETHGBP: 4.1 })
        return resolve()
      }
      t.context.clock.tick(uphold.interval + 1)
    })
  }).catch(error => t.fail(error))
  uphold.start()
  return assertions
})
