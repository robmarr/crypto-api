import test from 'ava'
import { mockBinanceApi, tinyMiniTickerResponse } from '../__mocks__/binance.mock.mjs'
import { BinancePure } from '../binance.mjs'

test('default cacheKey is binance', t => {
  const binance = new BinancePure()
  t.deepEqual(binance.cacheKey, 'binance')
})

test('respects cacheKey option', t => {
  const binance = new BinancePure({ cacheKey: 'pinance' })
  t.deepEqual(binance.cacheKey, 'pinance')
})

test('non-string cacheKey throws an Error', t => {
  t.throws(() => new BinancePure({ cacheKey: [] }), { instanceOf: TypeError })
})

test('starts the miniTicker connection when start called', t => {
  const Binance = mockBinanceApi(BinancePure)
  const binance = new Binance()
  binance.start()
  t.true(binance.dataSource.miniTicker.calledOnce)
  t.true(binance.openSockets.includes('miniTicker'))
})

test('stop calls terminate with correct args and clears openSockets', t => {
  const Binance = mockBinanceApi(BinancePure)
  const binance = new Binance()
  binance.start()
  binance.stop()
  t.true(binance.dataSource.terminate.calledOnceWithExactly('miniTicker'))
  t.is(binance.openSockets.length, 0)
})

test('emits data when received', t => {
  t.plan(2)
  const Binance = mockBinanceApi(BinancePure)
  const binance = new Binance()
  binance.on('data', payload => {
    t.is(payload.cacheKey, 'binance')
    t.like(payload.data, { ETHBTC: '0.05852700' })
  })
  binance.start()
  binance.dataSource.miniTicker.callArgWith(0, tinyMiniTickerResponse)
})

test('emits error when invalid data received', t => {
  t.plan(2)
  const Binance = mockBinanceApi(BinancePure)
  const binance = new Binance()
  binance.on('data', () => {
    t.fail('this test should not receive a data event')
  })
  binance.on('error', error => {
    t.true(error instanceof Error)
    t.truthy(error.message)
  })
  binance.start()
  binance.dataSource.miniTicker.callArgWith(0, "can't stop the funk")
})
