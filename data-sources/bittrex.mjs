import signalR from 'signalr-client'
import fetch from 'node-fetch'
import zlib from 'zlib'
import EventEmitter from 'events'

class BittrexAPI extends EventEmitter {
  constructor () {
    super()
    this.cacheKey = 'bittrex'
    this.apiURL = 'https://api.bittrex.com/v3/'
    this.wsURL = 'wss://socket-v3.bittrex.com/signalr'
    this.hub = 'c3'
    this.channels = ['heartbeat', 'tickers']
  }

  async connect () {
    return new Promise((resolve) => {
      const Client = signalR.client
      this.client = new Client(this.wsURL, [this.hub])
      this.client.serviceHandlers.messageReceived = this.messageReceived.bind(this)
      this.client.serviceHandlers.connected = () => {
        this.subscribe()
        return resolve(this.client)
      }
      this.client.serviceHandlers.reconnected = () => {
        this.subscribe()
        return resolve(this.client)
      }
    })
  }

  messageReceived (message) {
    const data = JSON.parse(message.utf8Data)
    switch (true) {
      case Array.isArray(data.R):
        this.channels.forEach((name, index) => {
          const { Success, ErrorCode } = data.R[index]
          console.log(`Subscription to "${name}" ${Success ? 'successful' : `failed: ${ErrorCode}`}`)
        })
        break
      case Array.isArray(data.M):
        data.M.forEach((m) => {
          if (m.A && m.A[0]) {
            const b64 = m.A[0]
            const raw = Buffer.from(b64, 'base64')
            zlib.inflateRaw(raw, (err, inflated) => {
              if (!err) {
                const markets = JSON.parse(inflated.toString('utf8'))
                const data = markets.deltas.reduce((prices, market) => {
                  const [symbol, price] =
                    this.constructor.parseMarketData(market)
                  prices[symbol] = price
                  return prices
                }, {})
                this.emit('data', { cacheKey: this.cacheKey, data })
              }
            })
          }
        })
        break
    }
  }

  subscribe () {
    return new Promise((resolve, reject) => {
      this.client.call(this.hub, 'subscribe', this.channels).done((err) => {
        if (err) {
          return reject(err)
        }
        resolve()
      })
    })
  }

  static parseMarketData (data) {
    const symbol = data.symbol.split('-').join('')
    const price = parseFloat(data.lastTradeRate)
    return [symbol, price]
  }

  async start () {
    const markets = await fetch(`${this.apiURL}/markets/tickers`).then(
      (response) => response.json()
    )
    const data = markets.reduce((prices, market) => {
      const [symbol, price] = this.constructor.parseMarketData(market)
      prices[symbol] = price
      return prices
    }, {})
    this.emit('data', { cacheKey: this.cacheKey, data })
    await this.connect(this.wsURL, this.hub)
  }

  stop () {
    this.client.end()
  }
}

export default BittrexAPI
