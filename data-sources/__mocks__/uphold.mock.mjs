import sinon from 'sinon'

// This mocks our use of the uphold api
export const mockUpholdAPI = (superclass) =>
  class extends superclass {
    constructor (opts = {}) {
      super(opts)
      this.url = 'https://example.com/v0'
      this.fetchTicker = sinon.stub()
      this.fetchTicker
        .withArgs('ETH')
        .onFirstCall()
        .resolves([
          {
            ask: '1.0',
            bid: '1.05',
            currency: 'ETH',
            pair: 'AEDETH'
          }
        ])
        .onSecondCall()
        .resolves([
          {
            ask: '1.1',
            bid: '1.15',
            currency: 'ETH',
            pair: 'AEDETH'
          }
        ])
        .withArgs('BTC')
        .onFirstCall()
        .resolves([
          {
            ask: '2.0',
            bid: '2.05',
            currency: 'BTC',
            pair: 'ETHBTC'
          }
        ])
        .onSecondCall()
        .resolves([
          {
            ask: '2.1',
            bid: '2.15',
            currency: 'BTC',
            pair: 'ETHBTC'
          }
        ])
        .withArgs('USD')
        .onFirstCall()
        .resolves([
          {
            ask: '3.0',
            bid: '3.05',
            currency: 'USD',
            pair: 'ETHUSD'
          }
        ])
        .onSecondCall()
        .resolves([
          {
            ask: '3.1',
            bid: '3.15',
            currency: 'USD',
            pair: 'ETHUSD'
          }
        ])
        .withArgs('GBP')
        .onFirstCall()
        .resolves([
          {
            ask: '4.0',
            bid: '4.05',
            currency: 'GBP',
            pair: 'ETHGBP'
          }
        ])
        .onSecondCall()
        .resolves([
          {
            ask: '4.1',
            bid: '4.15',
            currency: 'GBP',
            pair: 'ETHGBP'
          }
        ])
        .withArgs('FAIL').resolves('fail')
    }
  }

// these are mocks of responses returned by the ticker api endpoint
export const tinyTickerResponse = [
  {
    ask: '0.00051371919742575',
    bid: '0.000505567380074414',
    currency: 'ETH',
    pair: 'ADAETH'
  }
]

export const fullTickerResponse = [
  {
    ask: '0.033124780265867311',
    bid: '0.032925296984840554',
    currency: 'ETH',
    pair: 'AAPL-ETH'
  },
  {
    ask: '0.137742421624177693',
    bid: '0.135798448160740254',
    currency: 'ETH',
    pair: 'AAVE-ETH'
  },
  {
    ask: '0.00051371919742575',
    bid: '0.000505567380074414',
    currency: 'ETH',
    pair: 'ADAETH'
  },
  {
    ask: '0.125651362057280224',
    bid: '0.124837744628242747',
    currency: 'ETH',
    pair: 'ADBE-ETH'
  },
  {
    ask: '0.000071946346937038',
    bid: '0.000071459716681376',
    currency: 'ETH',
    pair: 'AEDETH'
  },
  {
    ask: '0.019523032202471094',
    bid: '0.019415028047781145',
    currency: 'ETH',
    pair: 'AMD-ETH'
  },
  {
    ask: '0.834605606831033733',
    bid: '0.829511264486149674',
    currency: 'ETH',
    pair: 'AMZN-ETH'
  },
  {
    ask: '0.000002811435366844',
    bid: '0.000002790286753276',
    currency: 'ETH',
    pair: 'ARSETH'
  },
  {
    ask: '0.006267597539516248',
    bid: '0.006189659999696849',
    currency: 'ETH',
    pair: 'ATOMETH'
  },
  {
    ask: '0.000204189402995264',
    bid: '0.000202810757789255',
    currency: 'ETH',
    pair: 'AUDETH'
  },
  {
    ask: '0.05926980925480179',
    bid: '0.058911308457601104',
    currency: 'ETH',
    pair: 'BA-ETH'
  },
  {
    ask: '0.055075094890563413',
    bid: '0.054746897436938463',
    currency: 'ETH',
    pair: 'BABA-ETH'
  },
  {
    ask: '0.011071511669991035',
    bid: '0.011000876305056454',
    currency: 'ETH',
    pair: 'BAC-ETH'
  },
  {
    ask: '0.016390002947651406',
    bid: '0.016132279329048901',
    currency: 'ETH',
    pair: 'BAL-ETH'
  },
  {
    ask: '0.000318119851410494',
    bid: '0.000314973979538207',
    currency: 'ETH',
    pair: 'BATETH'
  },
  {
    ask: '0.329103638377502746',
    bid: '0.325787646519316939',
    currency: 'ETH',
    pair: 'BCHETH'
  },
  {
    ask: '0.017211792104993216',
    bid: '0.017106927542923949',
    currency: 'ETH',
    pair: 'BMY-ETH'
  },
  {
    ask: '0.075668528972013117',
    bid: '0.07521425507161479',
    currency: 'ETH',
    pair: 'BRK.B-ETH'
  },
  {
    ask: '0.00005004354952981',
    bid: '0.000049702311216325',
    currency: 'ETH',
    pair: 'BRLETH'
  },
  {
    ask: '13.252579081453129136',
    bid: '13.118067069155803499',
    currency: 'ETH',
    pair: 'BTCETH'
  },
  {
    ask: '13.281485388746024183',
    bid: '13.146836974945239414',
    currency: 'ETH',
    pair: 'BTC0-ETH'
  },
  {
    ask: '0.02502829782292479',
    bid: '0.023500990497935935',
    currency: 'ETH',
    pair: 'BTGETH'
  },
  {
    ask: '0.019878035974018238',
    bid: '0.019760590086059134',
    currency: 'ETH',
    pair: 'C-ETH'
  },
  {
    ask: '0.000217768767809388',
    bid: '0.000216299771152083',
    currency: 'ETH',
    pair: 'CADETH'
  },
  {
    ask: '0.000291394056430287',
    bid: '0.000289430507019038',
    currency: 'ETH',
    pair: 'CHFETH'
  },
  {
    ask: '0.015205509589687258',
    bid: '0.015115366941244275',
    currency: 'ETH',
    pair: 'CMCSA-ETH'
  },
  {
    ask: '0.000040957201611806',
    bid: '0.000040679858569665',
    currency: 'ETH',
    pair: 'CNYETH'
  },
  {
    ask: '0.207960187224172649',
    bid: '0.205654341121430813',
    currency: 'ETH',
    pair: 'COMP-ETH'
  },
  {
    ask: '0.013835429819366479',
    bid: '0.01375175222848471',
    currency: 'ETH',
    pair: 'CSCO-ETH'
  },
  {
    ask: '0.000012523426648173',
    bid: '0.000012435430511543',
    currency: 'ETH',
    pair: 'CZKETH'
  },
  {
    ask: '0.000264478192580208',
    bid: '0.000262457735070696',
    currency: 'ETH',
    pair: 'DAI-ETH'
  },
  {
    ask: '0.097718920136387178',
    bid: '0.096540474529459804',
    currency: 'ETH',
    pair: 'DASHETH'
  },
  {
    ask: '0.053185321952643019',
    bid: '0.050367455673844861',
    currency: 'ETH',
    pair: 'DCRETH'
  },
  {
    ask: '0.000031049861769775',
    bid: '0.000030128265724882',
    currency: 'ETH',
    pair: 'DGBETH'
  },
  {
    ask: '0.066904242194623934',
    bid: '0.066496579509117222',
    currency: 'ETH',
    pair: 'DHR-ETH'
  },
  {
    ask: '0.047451221332400774',
    bid: '0.047169508551392053',
    currency: 'ETH',
    pair: 'DIS-ETH'
  },
  {
    ask: '0.000042900127837155',
    bid: '0.000042608361843587',
    currency: 'ETH',
    pair: 'DKKETH'
  },
  {
    ask: '0.000109263201721729',
    bid: '0.000107525880770106',
    currency: 'ETH',
    pair: 'DOGEETH'
  },
  {
    ask: '0.010470659094956464',
    bid: '0.010355715765658503',
    currency: 'ETH',
    pair: 'DOT-ETH'
  },
  {
    ask: '0.01378527275648383',
    bid: '0.01370183184400989',
    currency: 'ETH',
    pair: 'EEM-ETH'
  },
  {
    ask: '0.02062511222642822',
    bid: '0.020504141075868299',
    currency: 'ETH',
    pair: 'EFA-ETH'
  },
  {
    ask: '0.002781203857152054',
    bid: '0.002746296384999855',
    currency: 'ETH',
    pair: 'EOSETH'
  },
  {
    ask: '30.37179133',
    bid: '30.18885104',
    currency: 'AAPL',
    pair: 'ETH-AAPL'
  },
  {
    ask: '7.363854399987928442',
    bid: '7.259927538724733057',
    currency: 'AAVE',
    pair: 'ETH-AAVE'
  },
  {
    ask: '1977.97573154',
    bid: '1946.58871729',
    currency: 'ADA',
    pair: 'ETHADA'
  },
  {
    ask: '8.01042509',
    bid: '7.95849631',
    currency: 'ADBE',
    pair: 'ETH-ADBE'
  },
  {
    ask: '13993.93269',
    bid: '13899.22295',
    currency: 'AED',
    pair: 'ETHAED'
  },
  {
    ask: '51.50650235',
    bid: '51.221522',
    currency: 'AMD',
    pair: 'ETH-AMD'
  },
  {
    ask: '1.20553159',
    bid: '1.19813833',
    currency: 'AMZN',
    pair: 'ETH-AMZN'
  },
  {
    ask: '358386.10997',
    bid: '355690.18348',
    currency: 'ARS',
    pair: 'ETHARS'
  },
  {
    ask: '161.55976732',
    bid: '159.550744',
    currency: 'ATOM',
    pair: 'ETHATOM'
  },
  {
    ask: '4930.71323',
    bid: '4897.40797',
    currency: 'AUD',
    pair: 'ETHAUD'
  },
  {
    ask: '16.97468698',
    bid: '16.87197278',
    currency: 'BA',
    pair: 'ETH-BA'
  },
  {
    ask: '18.26589298',
    bid: '18.15701015',
    currency: 'BABA',
    pair: 'ETH-BABA'
  },
  {
    ask: '90.90185398',
    bid: '90.3218797',
    currency: 'BAC',
    pair: 'ETH-BAC'
  },
  {
    ask: '61.987520771434339495',
    bid: '61.012801717848086948',
    currency: 'BAL',
    pair: 'ETH-BAL'
  },
  {
    ask: '3174.865433221277768162',
    bid: '3143.469342029923571532',
    currency: 'BAT',
    pair: 'ETHBAT'
  },
  {
    ask: '3.06951162',
    bid: '3.03854913',
    currency: 'BCH',
    pair: 'ETHBCH'
  },
  {
    ask: '58.45587391',
    bid: '58.0996899',
    currency: 'BMY',
    pair: 'ETH-BMY'
  },
  {
    ask: '13.29536836',
    bid: '13.21550868',
    currency: 'BRK.B',
    pair: 'ETH-BRK.B'
  },
  {
    ask: '20119.82501',
    bid: '19982.57159',
    currency: 'BRL',
    pair: 'ETHBRL'
  },
  {
    ask: '0.0762354',
    bid: '0.07542108',
    currency: 'BTC',
    pair: 'ETHBTC'
  },
  {
    ask: '0.076063923353257511',
    bid: '0.075292783203851698',
    currency: 'BTC0',
    pair: 'ETH-BTC0'
  },
  {
    ask: '42.55141307',
    bid: '39.95476379',
    currency: 'BTG',
    pair: 'ETHBTG'
  },
  {
    ask: '50.60579815',
    bid: '50.30677146',
    currency: 'C',
    pair: 'ETH-C'
  },
  {
    ask: '4623.22161',
    bid: '4592.0113',
    currency: 'CAD',
    pair: 'ETHCAD'
  },
  {
    ask: '3455.06554',
    bid: '3431.75397',
    currency: 'CHF',
    pair: 'ETHCHF'
  },
  {
    ask: '66.15785585',
    bid: '65.7656312',
    currency: 'CMCSA',
    pair: 'ETH-CMCSA'
  },
  {
    ask: '24582.2025',
    bid: '24415.70996',
    currency: 'CNY',
    pair: 'ETHCNY'
  },
  {
    ask: '4.862528038781047807',
    bid: '4.808612712595998622',
    currency: 'COMP',
    pair: 'ETH-COMP'
  },
  {
    ask: '72.71801967',
    bid: '72.27817164',
    currency: 'CSCO',
    pair: 'ETH-CSCO'
  },
  {
    ask: '80415.39629',
    bid: '79850.31957',
    currency: 'CZK',
    pair: 'ETHCZK'
  },
  {
    ask: '3810.137276886268310651',
    bid: '3781.030073762064937229',
    currency: 'DAI',
    pair: 'ETH-DAI'
  },
  {
    ask: '10.35838377',
    bid: '10.23340224',
    currency: 'DASH',
    pair: 'ETHDASH'
  },
  {
    ask: '19.856218',
    bid: '18.800343',
    currency: 'DCR',
    pair: 'ETHDCR'
  },
  {
    ask: '33191.4226043221',
    bid: '32206.2625401818',
    currency: 'DGB',
    pair: 'ETHDGB'
  },
  {
    ask: '15.03839179',
    bid: '14.94670853',
    currency: 'DHR',
    pair: 'ETH-DHR'
  },
  {
    ask: '21.20013721',
    bid: '21.0742643',
    currency: 'DIS',
    pair: 'ETH-DIS'
  },
  {
    ask: '23469.57686',
    bid: '23309.92551',
    currency: 'DKK',
    pair: 'ETHDKK'
  },
  {
    ask: '9300.0865733752',
    bid: '9152.2121283501',
    currency: 'DOGE',
    pair: 'ETHDOGE'
  },
  {
    ask: '96.565029654076372561',
    bid: '95.504971648029656684',
    currency: 'DOT',
    pair: 'ETH-DOT'
  },
  {
    ask: '72.98295954',
    bid: '72.54117946',
    currency: 'EEM',
    pair: 'ETH-EEM'
  },
  {
    ask: '48.77066805',
    bid: '48.48458143',
    currency: 'EFA',
    pair: 'ETH-EFA'
  },
  {
    ask: '364.12679092',
    bid: '359.55650836',
    currency: 'EOS',
    pair: 'ETHEOS'
  },
  {
    ask: '3155.87113',
    bid: '3134.57748',
    currency: 'EUR',
    pair: 'ETHEUR'
  },
  {
    ask: '102.06655165',
    bid: '101.448857',
    currency: 'EWZ',
    pair: 'ETH-EWZ'
  },
  {
    ask: '12.43082173',
    bid: '12.35678415',
    currency: 'FB',
    pair: 'ETH-FB'
  },
  {
    ask: '33.096951833047225884',
    bid: '32.711711604352855123',
    currency: 'FIL',
    pair: 'ETH-FIL'
  },
  {
    ask: '86.73794775',
    bid: '86.19110752',
    currency: 'FXI',
    pair: 'ETH-FXI'
  },
  {
    ask: '2714.52036',
    bid: '2696.1806',
    currency: 'GBP',
    pair: 'ETHGBP'
  },
  {
    ask: '103.11732881',
    bid: '102.49187262',
    currency: 'GDX',
    pair: 'ETH-GDX'
  },
  {
    ask: '22.28174346',
    bid: '22.14872116',
    currency: 'GLD',
    pair: 'ETH-GLD'
  },
  {
    ask: '1.68555193',
    bid: '1.67479503',
    currency: 'GOOG',
    pair: 'ETH-GOOG'
  },
  {
    ask: '1.7105958',
    bid: '1.69956921',
    currency: 'GOOGL',
    pair: 'ETH-GOOGL'
  },
  {
    ask: '2922.904757516415163113',
    bid: '2883.557460283628554714',
    currency: 'GRT',
    pair: 'ETH-GRT'
  },
  {
    ask: '14236.782178',
    bid: '13976.68204',
    currency: 'HBAR',
    pair: 'ETH-HBAR'
  },
  {
    ask: '11.7268528',
    bid: '11.65640223',
    currency: 'HD',
    pair: 'ETH-HD'
  },
  {
    ask: '29598.43791',
    bid: '29397.02437',
    currency: 'HKD',
    pair: 'ETHHKD'
  },
  {
    ask: '256.488457191839672324',
    bid: '252.327075947899972253',
    currency: 'HNT',
    pair: 'ETH-HNT'
  },
  {
    ask: '23756.70606',
    bid: '23594.48763',
    currency: 'HRK',
    pair: 'ETHHRK'
  },
  {
    ask: '1129394.80522',
    bid: '1117433.80251',
    currency: 'HUF',
    pair: 'ETHHUF'
  },
  {
    ask: '43.83348452',
    bid: '43.57141495',
    currency: 'HYG',
    pair: 'ETH-HYG'
  },
  {
    ask: '12522.01494',
    bid: '12437.09188',
    currency: 'ILS',
    pair: 'ETHILS'
  },
  {
    ask: '280063.32378',
    bid: '277923.73593',
    currency: 'INR',
    pair: 'ETHINR'
  },
  {
    ask: '70.82362921',
    bid: '70.39768274',
    currency: 'INTC',
    pair: 'ETH-INTC'
  },
  {
    ask: '1970.54097309',
    bid: '1943.74203545',
    currency: 'IOTA',
    pair: 'ETHIOTA'
  },
  {
    ask: '9.23871691',
    bid: '9.18527269',
    currency: 'IVV',
    pair: 'ETH-IVV'
  },
  {
    ask: '17.729581',
    bid: '17.63322948',
    currency: 'IWM',
    pair: 'ETH-IWM'
  },
  {
    ask: '22.41975121',
    bid: '22.29657072',
    currency: 'JNJ',
    pair: 'ETH-JNJ'
  },
  {
    ask: '23.6269194',
    bid: '23.48770107',
    currency: 'JPM',
    pair: 'ETH-JPM'
  },
  {
    ask: '417789.31232',
    bid: '414453.01828',
    currency: 'JPY',
    pair: 'ETHJPY'
  },
  {
    ask: '408814.24122',
    bid: '405578.21831',
    currency: 'KES',
    pair: 'ETHKES'
  },
  {
    ask: '627028.077429983525538443',
    bid: '613954.716369529983789302',
    currency: 'LBA',
    pair: 'ETHLBA'
  },
  {
    ask: '87.40773826',
    bid: '86.54139318',
    currency: 'LINK',
    pair: 'ETHLINK'
  },
  {
    ask: '29.20999138',
    bid: '29.03870793',
    currency: 'LQD',
    pair: 'ETH-LQD'
  },
  {
    ask: '12.03293618',
    bid: '11.91225055',
    currency: 'LTC',
    pair: 'ETHLTC'
  },
  {
    ask: '10.56448194',
    bid: '10.49682676',
    currency: 'MA',
    pair: 'ETH-MA'
  },
  {
    ask: '0.815779797324895882',
    bid: '0.805086277602093371',
    currency: 'MKR',
    pair: 'ETH-MKR'
  },
  {
    ask: '15.61653237',
    bid: '15.52499997',
    currency: 'MSFT',
    pair: 'ETH-MSFT'
  },
  {
    ask: '49.0219061',
    bid: '48.73406574',
    currency: 'MU',
    pair: 'ETH-MU'
  },
  {
    ask: '76442.28786',
    bid: '75913.83907',
    currency: 'MXN',
    pair: 'ETHMXN'
  },
  {
    ask: '288.012205',
    bid: '281.008878',
    currency: 'NANO',
    pair: 'ETHNANO'
  },
  {
    ask: '40.150132',
    bid: '39.396246',
    currency: 'NEO',
    pair: 'ETHNEO'
  },
  {
    ask: '7.81924667',
    bid: '7.77375064',
    currency: 'NFLX',
    pair: 'ETH-NFLX'
  },
  {
    ask: '31900.60968',
    bid: '31683.67341',
    currency: 'NOK',
    pair: 'ETHNOK'
  },
  {
    ask: '6.93589811',
    bid: '6.89282785',
    currency: 'NVDA',
    pair: 'ETH-NVDA'
  },
  {
    ask: '5303.74521',
    bid: '5267.95998',
    currency: 'NZD',
    pair: 'ETHNZD'
  },
  {
    ask: '387.6547908',
    bid: '383.29884556',
    currency: 'OMG',
    pair: 'ETHOMG'
  },
  {
    ask: '6718.31609529',
    bid: '6641.71227648',
    currency: 'OXT',
    pair: 'ETH-OXT'
  },
  {
    ask: '27.73088018',
    bid: '27.56987192',
    currency: 'PG',
    pair: 'ETH-PG'
  },
  {
    ask: '182457.36078',
    bid: '181162.12309',
    currency: 'PHP',
    pair: 'ETHPHP'
  },
  {
    ask: '14280.07231',
    bid: '14183.36838',
    currency: 'PLN',
    pair: 'ETHPLN'
  },
  {
    ask: '11.90101812',
    bid: '11.83224586',
    currency: 'QQQ',
    pair: 'ETH-QQQ'
  },
  {
    ask: '4636.617771388892273319',
    bid: '4540.889213876435472037',
    currency: 'REN',
    pair: 'ETH-REN'
  },
  {
    ask: '12.07506926',
    bid: '11.98308803',
    currency: 'ROKU',
    pair: 'ETH-ROKU'
  },
  {
    ask: '15550.83977',
    bid: '15445.22257',
    currency: 'RON',
    pair: 'ETHRON'
  },
  {
    ask: '32061.87246',
    bid: '31843.4555',
    currency: 'SEK',
    pair: 'ETHSEK'
  },
  {
    ask: '5081.01456',
    bid: '5046.65914',
    currency: 'SGD',
    pair: 'ETHSGD'
  },
  {
    ask: '190.00755974459715292',
    bid: '186.935979581685101645',
    currency: 'SNX',
    pair: 'ETH-SNX'
  },
  {
    ask: '91.894893564417394222',
    bid: '90.745359425205216691',
    currency: 'SOL',
    pair: 'ETH-SOL'
  },
  {
    ask: '9.27358042',
    bid: '9.22012321',
    currency: 'SPY',
    pair: 'ETH-SPY'
  },
  {
    ask: '100450.26210344',
    bid: '95466.24494745',
    currency: 'STORM',
    pair: 'ETHSTORM'
  },
  {
    ask: '118.20067017',
    bid: '117.46047637',
    currency: 'T',
    pair: 'ETH-T'
  },
  {
    ask: '27.99604841',
    bid: '27.8331828',
    currency: 'TLT',
    pair: 'ETH-TLT'
  },
  {
    ask: '42.00950618',
    bid: '41.750475',
    currency: 'TQQQ',
    pair: 'ETH-TQQQ'
  },
  {
    ask: '31908.621981897',
    bid: '31504.4960077325',
    currency: 'TRX',
    pair: 'ETHTRX'
  },
  {
    ask: '6.58102103',
    bid: '6.54144364',
    currency: 'TSLA',
    pair: 'ETH-TSLA'
  },
  {
    ask: '3806.441074107410744843',
    bid: '3787.721827817218274421',
    currency: 'TUSD',
    pair: 'ETH-TUSD'
  },
  {
    ask: '151.557196801819616362',
    bid: '149.634085644673513703',
    currency: 'UMA',
    pair: 'ETH-UMA'
  },
  {
    ask: '9.24431182',
    bid: '9.18527269',
    currency: 'UNH',
    pair: 'ETH-UNH'
  },
  {
    ask: '101.302712772243394577',
    bid: '100.157466199735863792',
    currency: 'UNI',
    pair: 'ETH-UNI'
  },
  {
    ask: '0.0762354',
    bid: '0.07542108',
    currency: 'UPBTC',
    pair: 'ETHUPBTC'
  },
  {
    ask: '490.789199972146933774',
    bid: '445.658894117647055926',
    currency: 'UPCO2',
    pair: 'ETH-UPCO2'
  },
  {
    ask: '3155.87113',
    bid: '3134.57748',
    currency: 'UPEUR',
    pair: 'ETHUPEUR'
  },
  {
    ask: '211683.00502834',
    bid: '197502.63813248',
    currency: 'UPT',
    pair: 'ETHUPT'
  },
  {
    ask: '3806.06043',
    bid: '3788.1006',
    currency: 'UPUSD',
    pair: 'ETHUPUSD'
  },
  {
    ask: '2.10779627',
    bid: '2.05333993',
    currency: 'UPXAU',
    pair: 'ETH-UPXAU'
  },
  {
    ask: '3806.06043',
    bid: '3788.1006',
    currency: 'USD',
    pair: 'ETHUSD'
  },
  {
    ask: '3806.444843',
    bid: '3787.721789',
    currency: 'USDC',
    pair: 'ETH-USDC'
  },
  {
    ask: '3808.842661',
    bid: '3778.952337',
    currency: 'USDT',
    pair: 'ETH-USDT'
  },
  {
    ask: '17.10668115',
    bid: '17.0037608',
    currency: 'V',
    pair: 'ETH-V'
  },
  {
    ask: '953899.85715793',
    bid: '947025.15',
    currency: 'VCO',
    pair: 'ETH-VCO'
  },
  {
    ask: '380606043',
    bid: '1191226.60375452',
    currency: 'VOX',
    pair: 'ETHVOX'
  },
  {
    ask: '0.07646376',
    bid: '0.07526955',
    currency: 'WBTC',
    pair: 'ETH-WBTC'
  },
  {
    ask: '81.83319186',
    bid: '81.32461044',
    currency: 'WFC',
    pair: 'ETH-WFC'
  },
  {
    ask: '141.83668605',
    bid: '133.05748814',
    currency: 'XAG',
    pair: 'ETHXAG'
  },
  {
    ask: '2.10269615',
    bid: '2.04534703',
    currency: 'XAU',
    pair: 'ETHXAU'
  },
  {
    ask: '12510.88171437',
    bid: '12225.59496026',
    currency: 'XEM',
    pair: 'ETHXEM'
  },
  {
    ask: '102.14872449',
    bid: '101.5304148',
    currency: 'XLF',
    pair: 'ETH-XLF'
  },
  {
    ask: '6347.77171558',
    bid: '6256.46290003',
    currency: 'XLM',
    pair: 'ETHXLM'
  },
  {
    ask: '58.16996265',
    bid: '57.81592328',
    currency: 'XLU',
    pair: 'ETH-XLU'
  },
  {
    ask: '63.77450081',
    bid: '63.39916688',
    currency: 'XOM',
    pair: 'ETH-XOM'
  },
  {
    ask: '1.36283606',
    bid: '1.29662895',
    currency: 'XPD',
    pair: 'ETHXPD'
  },
  {
    ask: '3.15472931',
    bid: '3.00203184',
    currency: 'XPT',
    pair: 'ETHXPT'
  },
  {
    ask: '2897.40537',
    bid: '2866.186768',
    currency: 'XRP',
    pair: 'ETHXRP'
  },
  {
    ask: '622.751414',
    bid: '615.960309',
    currency: 'XTZ',
    pair: 'ETH-XTZ'
  },
  {
    ask: '20058.2894864935',
    bid: '19830.9108991724',
    currency: 'ZIL',
    pair: 'ETHZIL'
  },
  {
    ask: '2286.55390781',
    bid: '2248.10421343',
    currency: 'ZRX',
    pair: 'ETHZRX'
  },
  {
    ask: '0.000319020038696967',
    bid: '0.00031687095414825',
    currency: 'ETH',
    pair: 'EURETH'
  },
  {
    ask: '0.009857182779147955',
    bid: '0.009797532300347628',
    currency: 'ETH',
    pair: 'EWZ-ETH'
  },
  {
    ask: '0.080927101038446627',
    bid: '0.080445385886844644',
    currency: 'ETH',
    pair: 'FB-ETH'
  },
  {
    ask: '0.030570090984384129',
    bid: '0.030214262783000517',
    currency: 'ETH',
    pair: 'FIL-ETH'
  },
  {
    ask: '0.011602120598381163',
    bid: '0.011528981425026922',
    currency: 'ETH',
    pair: 'FXI-ETH'
  },
  {
    ask: '0.000370893001099285',
    bid: '0.000368394045703577',
    currency: 'ETH',
    pair: 'GBPETH'
  },
  {
    ask: '0.009756868653382657',
    bid: '0.009697691531397987',
    currency: 'ETH',
    pair: 'GDX-ETH'
  },
  {
    ask: '0.045149276130628675',
    bid: '0.044879854942292632',
    currency: 'ETH',
    pair: 'GLD-ETH'
  },
  {
    ask: '0.597077596091297611',
    bid: '0.593282750373986828',
    currency: 'ETH',
    pair: 'GOOG-ETH'
  },
  {
    ask: '0.588379305449280334',
    bid: '0.584601858252681233',
    currency: 'ETH',
    pair: 'GOOGL-ETH'
  },
  {
    ask: '0.00034679385230688',
    bid: '0.000342125413915196',
    currency: 'ETH',
    pair: 'GRT-ETH'
  },
  {
    ask: '0.000071547730279392',
    bid: '0.000070240608344728',
    currency: 'ETH',
    pair: 'HBAR-ETH'
  },
  {
    ask: '0.085789696292648698',
    bid: '0.085274526237619367',
    currency: 'ETH',
    pair: 'HD-ETH'
  },
  {
    ask: '0.000034017048016096',
    bid: '0.000033785590734827',
    currency: 'ETH',
    pair: 'HKDETH'
  },
  {
    ask: '0.003963110166609624',
    bid: '0.003898810928758688',
    currency: 'ETH',
    pair: 'HNT-ETH'
  },
  {
    ask: '0.000042382718135839',
    bid: '0.000042093393666899',
    currency: 'ETH',
    pair: 'HRKETH'
  },
  {
    ask: '8.94907595644e-7',
    bid: '8.85429977263e-7',
    currency: 'ETH',
    pair: 'HUFETH'
  },
  {
    ask: '0.022950816036934205',
    bid: '0.022813615704992881',
    currency: 'ETH',
    pair: 'HYG-ETH'
  },
  {
    ask: '0.000080404411646302',
    bid: '0.000079859478216429',
    currency: 'ETH',
    pair: 'ILSETH'
  },
  {
    ask: '0.000003598109300477',
    bid: '0.000003570621184277',
    currency: 'ETH',
    pair: 'INRETH'
  },
  {
    ask: '0.014205008177449156',
    bid: '0.014119586640404439',
    currency: 'ETH',
    pair: 'INTC-ETH'
  },
  {
    ask: '0.00051447155336899',
    bid: '0.000507474864239083',
    currency: 'ETH',
    pair: 'IOTAETH'
  },
  {
    ask: '0.108869864754911839',
    bid: '0.108240530484693279',
    currency: 'ETH',
    pair: 'IVV-ETH'
  },
  {
    ask: '0.056711015013698476',
    bid: '0.05640291948806496',
    currency: 'ETH',
    pair: 'IWM-ETH'
  },
  {
    ask: '0.044849922940272537',
    bid: '0.04460358502505435',
    currency: 'ETH',
    pair: 'JNJ-ETH'
  },
  {
    ask: '0.042575426851124322',
    bid: '0.042324603868677914',
    currency: 'ETH',
    pair: 'JPM-ETH'
  },
  {
    ask: '0.000002412818709197',
    bid: '0.000002393551066134',
    currency: 'ETH',
    pair: 'JPYETH'
  },
  {
    ask: '0.000002465615617495',
    bid: '0.000002446098839266',
    currency: 'ETH',
    pair: 'KESETH'
  },
  {
    ask: '0.000001628784620979',
    bid: '0.000001594824914537',
    currency: 'ETH',
    pair: 'LBAETH'
  },
  {
    ask: '0.011555160388295929',
    bid: '0.011440635481449773',
    currency: 'ETH',
    pair: 'LINKETH'
  },
  {
    ask: '0.034436783437060812',
    bid: '0.034234874195100454',
    currency: 'ETH',
    pair: 'LQD-ETH'
  },
  {
    ask: '0.083947136989973366',
    bid: '0.083105464513079172',
    currency: 'ETH',
    pair: 'LTCETH'
  },
  {
    ask: '0.095266741332054471',
    bid: '0.094656931130229016',
    currency: 'ETH',
    pair: 'MA-ETH'
  },
  {
    ask: '1.242102899801553433',
    bid: '1.225820991496973648',
    currency: 'ETH',
    pair: 'MKR-ETH'
  },
  {
    ask: '0.064412228122980744',
    bid: '0.06403471633791161',
    currency: 'ETH',
    pair: 'MSFT-ETH'
  },
  {
    ask: '0.020519518409833169',
    bid: '0.02039904552960552',
    currency: 'ETH',
    pair: 'MU-ETH'
  },
  {
    ask: '0.000013172828620233',
    bid: '0.000013081768121059',
    currency: 'ETH',
    pair: 'MXNETH'
  },
  {
    ask: '0.003558577615388571',
    bid: '0.003472078345324641',
    currency: 'ETH',
    pair: 'NANOETH'
  },
  {
    ask: '0.025382850180905991',
    bid: '0.024907741677659058',
    currency: 'ETH',
    pair: 'NEOETH'
  },
  {
    ask: '0.128637695102395238',
    bid: '0.127890068734405191',
    currency: 'ETH',
    pair: 'NFLX-ETH'
  },
  {
    ask: '0.000031561991780261',
    bid: '0.00003134737406153',
    currency: 'ETH',
    pair: 'NOKETH'
  },
  {
    ask: '0.145077984465354621',
    bid: '0.144177952529250763',
    currency: 'ETH',
    pair: 'NVDA-ETH'
  },
  {
    ask: '0.000189826004092923',
    bid: '0.000188546664772739',
    currency: 'ETH',
    pair: 'NZDETH'
  },
  {
    ask: '0.002608930185222645',
    bid: '0.002579614848627086',
    currency: 'ETH',
    pair: 'OMGETH'
  },
  {
    ask: '0.000150563583237468',
    bid: '0.000148846822171974',
    currency: 'ETH',
    pair: 'OXT-ETH'
  },
  {
    ask: '0.036271476000399813',
    bid: '0.03606090931141625',
    currency: 'ETH',
    pair: 'PG-ETH'
  },
  {
    ask: '0.000005519916762507',
    bid: '0.000005480732737603',
    currency: 'ETH',
    pair: 'PHPETH'
  },
  {
    ask: '0.000070504991340516',
    bid: '0.000070027789863546',
    currency: 'ETH',
    pair: 'PLNETH'
  },
  {
    ask: '0.084514650957263464',
    bid: '0.084026516625748859',
    currency: 'ETH',
    pair: 'QQQ-ETH'
  },
  {
    ask: '0.000220221184199808',
    bid: '0.000215674452651819',
    currency: 'ETH',
    pair: 'REN-ETH'
  },
  {
    ask: '0.083450793255068332',
    bid: '0.082815290455070324',
    currency: 'ETH',
    pair: 'ROKU-ETH'
  },
  {
    ask: '0.000064744848645256',
    bid: '0.000064305337369538',
    currency: 'ETH',
    pair: 'RONETH'
  },
  {
    ask: '0.000031403601055368',
    bid: '0.000031189730742136',
    currency: 'ETH',
    pair: 'SEKETH'
  },
  {
    ask: '0.000198149436686027',
    bid: '0.000196812429486307',
    currency: 'ETH',
    pair: 'SGDETH'
  },
  {
    ask: '0.005349424986231892',
    bid: '0.005262948491860911',
    currency: 'ETH',
    pair: 'SNX-ETH'
  },
  {
    ask: '0.011019847255376492',
    bid: '0.010881997477901311',
    currency: 'ETH',
    pair: 'SOL-ETH'
  },
  {
    ask: '0.108458048870191143',
    bid: '0.107833285242925008',
    currency: 'ETH',
    pair: 'SPY-ETH'
  },
  {
    ask: '0.000010474906606229',
    bid: '0.000009955175619741',
    currency: 'ETH',
    pair: 'STORMETH'
  },
  {
    ask: '0.008513501462975939',
    bid: '0.008460191474153757',
    currency: 'ETH',
    pair: 'T-ETH'
  },
  {
    ask: '0.035928296096465899',
    bid: '0.035719348786062216',
    currency: 'ETH',
    pair: 'TLT-ETH'
  },
  {
    ask: '0.023951811100264889',
    bid: '0.023804141228519579',
    currency: 'ETH',
    pair: 'TQQQ-ETH'
  },
  {
    ask: '0.000031741501268473',
    bid: '0.00003133949189556',
    currency: 'ETH',
    pair: 'TRXETH'
  },
  {
    ask: '0.152870808130069341',
    bid: '0.151952395564039884',
    currency: 'ETH',
    pair: 'TSLA-ETH'
  },
  {
    ask: '0.000264010939941775',
    bid: '0.000262712591770383',
    currency: 'ETH',
    pair: 'TUSD-ETH'
  },
  {
    ask: '0.006682969296010783',
    bid: '0.006598169015408929',
    currency: 'ETH',
    pair: 'UMA-ETH'
  },
  {
    ask: '0.108869864754911839',
    bid: '0.108174845768279042',
    currency: 'ETH',
    pair: 'UNH-ETH'
  },
  {
    ask: '0.009984278136647173',
    bid: '0.009871403959815735',
    currency: 'ETH',
    pair: 'UNI-ETH'
  },
  {
    ask: '13.252579081453129136',
    bid: '13.118067069155803499',
    currency: 'ETH',
    pair: 'UPBTCETH'
  },
  {
    ask: '0.002243868602644821',
    bid: '0.002037534648392326',
    currency: 'ETH',
    pair: 'UPCO2-ETH'
  },
  {
    ask: '0.000319020038696967',
    bid: '0.00031687095414825',
    currency: 'ETH',
    pair: 'UPEURETH'
  },
  {
    ask: '0.000005063223505733',
    bid: '0.000004724044804511',
    currency: 'ETH',
    pair: 'UPTETH'
  },
  {
    ask: '0.000263984541487626',
    bid: '0.000262738865656949',
    currency: 'ETH',
    pair: 'UPUSDETH'
  },
  {
    ask: '0.487002457115315718',
    bid: '0.474430738347472632',
    currency: 'ETH',
    pair: 'UPXAU-ETH'
  },
  {
    ask: '0.000263984541487626',
    bid: '0.000262738865656949',
    currency: 'ETH',
    pair: 'USDETH'
  },
  {
    ask: '0.000264010939941775',
    bid: '0.000262712591770383',
    currency: 'ETH',
    pair: 'USDC-ETH'
  },
  {
    ask: '0.000264623384078027',
    bid: '0.000262547066285019',
    currency: 'ETH',
    pair: 'USDT-ETH'
  },
  {
    ask: '0.058810476152613321',
    bid: '0.058456770220014583',
    currency: 'ETH',
    pair: 'V-ETH'
  },
  {
    ask: '0.000001055938165951',
    bid: '0.000001048328073971',
    currency: 'ETH',
    pair: 'VCO-ETH'
  },
  {
    ask: '8.39470841931e-7',
    bid: '2.627388656e-9',
    currency: 'ETH',
    pair: 'VOXETH'
  },
  {
    ask: '13.281344006545139655',
    bid: '13.081419661011524524',
    currency: 'ETH',
    pair: 'WBTC-ETH'
  },
  {
    ask: '0.01229639994249362',
    bid: '0.012219984641704697',
    currency: 'ETH',
    pair: 'WFC-ETH'
  },
  {
    ask: '0.007515547501563192',
    bid: '0.007050363622313791',
    currency: 'ETH',
    pair: 'XAGETH'
  },
  {
    ask: '0.48891458162436387',
    bid: '0.475581266585407098',
    currency: 'ETH',
    pair: 'XAUETH'
  },
  {
    ask: '0.000081795610179941',
    bid: '0.000079930417710157',
    currency: 'ETH',
    pair: 'XEMETH'
  },
  {
    ask: '0.009849263242903327',
    bid: '0.009789650134377919',
    currency: 'ETH',
    pair: 'XLF-ETH'
  },
  {
    ask: '0.000159834720334513',
    bid: '0.00015753559645925',
    currency: 'ETH',
    pair: 'XLMETH'
  },
  {
    ask: '0.017296267158269256',
    bid: '0.017191003979934173',
    currency: 'ETH',
    pair: 'XLU-ETH'
  },
  {
    ask: '0.015773076353885654',
    bid: '0.015680255502406716',
    currency: 'ETH',
    pair: 'XOM-ETH'
  },
  {
    ask: '0.77120821448089387',
    bid: '0.733769537127396247',
    currency: 'ETH',
    pair: 'XPDETH'
  },
  {
    ask: '0.333104466655400121',
    bid: '0.316984793643962045',
    currency: 'ETH',
    pair: 'XPTETH'
  },
  {
    ask: '0.000348895169257121',
    bid: '0.000345136401315624',
    currency: 'ETH',
    pair: 'XRPETH'
  },
  {
    ask: '0.001623473252003922',
    bid: '0.001605783752624231',
    currency: 'ETH',
    pair: 'XTZ-ETH'
  },
  {
    ask: '0.000050426327114967',
    bid: '0.000049854699758406',
    currency: 'ETH',
    pair: 'ZILETH'
  },
  {
    ask: '0.00044481923209748',
    bid: '0.000437339351440617',
    currency: 'ETH',
    pair: 'ZRXETH'
  }
]
