import sinon from 'sinon'

// This mocks the elements of node-binance-api we're using
export const mockBinanceApi = superclass => class extends superclass {
  constructor () {
    super()
    this.dataSource = {
      miniTicker: sinon.stub().returns('miniTicker'),
      terminate: sinon.stub()
    }
  }
}

// thee are mocks of responses returned by the mini ticker
export const tinyMiniTickerResponse = {
  ETHBTC: {
    close: '0.05852700',
    open: '0.06166000',
    high: '0.06278400',
    low: '0.05834000',
    volume: '381985.48400000',
    quoteVolume: '23178.54222318',
    eventTime: 1620227052818
  }
}

export const fullMiniTickerResponse = {
  ETHBTC: {
    close: '0.05852700',
    open: '0.06166000',
    high: '0.06278400',
    low: '0.05834000',
    volume: '381985.48400000',
    quoteVolume: '23178.54222318',
    eventTime: 1620227052818
  },
  BNBBTC: {
    close: '0.01135600',
    open: '0.01138600',
    high: '0.01181100',
    low: '0.01130000',
    volume: '578143.20000000',
    quoteVolume: '6674.23592789',
    eventTime: 1620227053127
  },
  BTCUSDT: {
    close: '57204.50000000',
    open: '54820.01000000',
    high: '57333.00000000',
    low: '52900.00000000',
    volume: '77470.84177800',
    quoteVolume: '4239423055.03509467',
    eventTime: 1620227053307
  },
  ETHUSDT: {
    close: '3348.87000000',
    open: '3379.53000000',
    high: '3429.31000000',
    low: '3160.00000000',
    volume: '1432754.87689000',
    quoteVolume: '4748560425.11307760',
    eventTime: 1620227053268
  },
  KNCBTC: {
    close: '0.00005625',
    open: '0.00005669',
    high: '0.00005767',
    low: '0.00005486',
    volume: '1909707.00000000',
    quoteVolume: '108.07820738',
    eventTime: 1620227053165
  },
  LINKBTC: {
    close: '0.00083670',
    open: '0.00080426',
    high: '0.00093836',
    low: '0.00077600',
    volume: '5818997.90000000',
    quoteVolume: '5034.11870437',
    eventTime: 1620227052360
  },
  XVGBTC: {
    close: '0.00000113',
    open: '0.00000091',
    high: '0.00000115',
    low: '0.00000087',
    volume: '1118667212.00000000',
    quoteVolume: '1141.17396456',
    eventTime: 1620227052991
  },
  EOSBTC: {
    close: '0.00013230',
    open: '0.00012200',
    high: '0.00013930',
    low: '0.00011690',
    volume: '12673830.04000000',
    quoteVolume: '1643.32444048',
    eventTime: 1620227053174
  },
  ETCETH: {
    close: '0.02312600',
    open: '0.01795900',
    high: '0.02498500',
    low: '0.01727900',
    volume: '638390.21000000',
    quoteVolume: '13827.80897810',
    eventTime: 1620227052481
  },
  TRXETH: {
    close: '0.00003972',
    open: '0.00003661',
    high: '0.00003990',
    low: '0.00003600',
    volume: '86034741.00000000',
    quoteVolume: '3237.33677659',
    eventTime: 1620227052986
  },
  ARKBTC: {
    close: '0.00003399',
    open: '0.00003457',
    high: '0.00003830',
    low: '0.00003340',
    volume: '1814571.00000000',
    quoteVolume: '63.28674106',
    eventTime: 1620227052636
  },
  ENJBTC: {
    close: '0.00004446',
    open: '0.00004373',
    high: '0.00004758',
    low: '0.00004280',
    volume: '7355781.00000000',
    quoteVolume: '328.03932132',
    eventTime: 1620227052727
  },
  BNBUSDT: {
    close: '650.06000000',
    open: '624.44000000',
    high: '656.67000000',
    low: '604.14000000',
    volume: '2844635.13610000',
    quoteVolume: '1792133538.06280600',
    eventTime: 1620227052984
  },
  BATBTC: {
    close: '0.00002272',
    open: '0.00002169',
    high: '0.00002292',
    low: '0.00002125',
    volume: '7682384.00000000',
    quoteVolume: '170.58371198',
    eventTime: 1620227053244
  },
  LTCETH: {
    close: '0.10286000',
    open: '0.09131000',
    high: '0.10539000',
    low: '0.08930000',
    volume: '100928.62200000',
    quoteVolume: '9939.93217035',
    eventTime: 1620227052958
  },
  LTCUSDT: {
    close: '344.10000000',
    open: '308.51000000',
    high: '352.94000000',
    low: '292.17000000',
    volume: '3978011.87333000',
    quoteVolume: '1289285318.78476630',
    eventTime: 1620227053058
  },
  WAVESBTC: {
    close: '0.00061850',
    open: '0.00064090',
    high: '0.00072790',
    low: '0.00061580',
    volume: '1351909.04000000',
    quoteVolume: '895.81177036',
    eventTime: 1620227053091
  },
  XEMBTC: {
    close: '0.00000590',
    open: '0.00000578',
    high: '0.00000607',
    low: '0.00000564',
    volume: '27365019.00000000',
    quoteVolume: '160.22981613',
    eventTime: 1620227052934
  },
  SYSBTC: {
    close: '0.00001418',
    open: '0.00001049',
    high: '0.00001600',
    low: '0.00001018',
    volume: '31376575.00000000',
    quoteVolume: '417.77372093',
    eventTime: 1620227053296
  },
  ADAUSDT: {
    close: '1.39710000',
    open: '1.29800000',
    high: '1.39900000',
    low: '1.25570000',
    volume: '425567442.97000000',
    quoteVolume: '563188301.17907700',
    eventTime: 1620227053114
  },
  XRPUSDT: {
    close: '1.56920000',
    open: '1.40330000',
    high: '1.60600000',
    low: '1.32000000',
    volume: '1414035234.98000000',
    quoteVolume: '2055824373.19021400',
    eventTime: 1620227053225
  },
  SKYBTC: {
    close: '0.00006014',
    open: '0.00005925',
    high: '0.00006135',
    low: '0.00005603',
    volume: '853540.00000000',
    quoteVolume: '50.63415659',
    eventTime: 1620227053178
  },
  EOSUSDT: {
    close: '7.56800000',
    open: '6.69190000',
    high: '7.70510000',
    low: '6.20170000',
    volume: '81169017.97000000',
    quoteVolume: '571725198.77458500',
    eventTime: 1620227052994
  },
  IOTAUSDT: {
    close: '1.98870000',
    open: '1.88260000',
    high: '2.01290000',
    low: '1.80690000',
    volume: '32160888.58000000',
    quoteVolume: '61537378.08015100',
    eventTime: 1620227052728
  },
  XLMUSDT: {
    close: '0.58083000',
    open: '0.51699000',
    high: '0.59088000',
    low: '0.49365000',
    volume: '385227678.30000000',
    quoteVolume: '207806259.57056300',
    eventTime: 1620227053042
  },
  TRXUSDT: {
    close: '0.13301000',
    open: '0.12375000',
    high: '0.13364000',
    low: '0.11762000',
    volume: '2888870852.90000000',
    quoteVolume: '362977500.07841500',
    eventTime: 1620227053298
  },
  ETCUSDT: {
    close: '77.47700000',
    open: '60.56200000',
    high: '81.67800000',
    low: '56.57000000',
    volume: '26973568.39500000',
    quoteVolume: '1924515479.77275400',
    eventTime: 1620227053124
  },
  VETUSDT: {
    close: '0.20227000',
    open: '0.19172000',
    high: '0.20610000',
    low: '0.18456000',
    volume: '2159722468.00000000',
    quoteVolume: '418532502.10252500',
    eventTime: 1620227052972
  },
  PAXUSDT: {
    close: '0.99900000',
    open: '0.99940000',
    high: '1.00000000',
    low: '0.99800000',
    volume: '12377715.19000000',
    quoteVolume: '12369774.54926100',
    eventTime: 1620227052316
  },
  BTCUSDC: {
    close: '57270.03000000',
    open: '54842.14000000',
    high: '57387.23000000',
    low: '52915.12000000',
    volume: '3143.82881700',
    quoteVolume: '172328116.18999279',
    eventTime: 1620227053241
  },
  LINKUSDT: {
    close: '47.87700000',
    open: '44.09800000',
    high: '51.20000000',
    low: '41.45800000',
    volume: '23736191.47500000',
    quoteVolume: '1120602427.73853450',
    eventTime: 1620227052716
  },
  WAVESUSDT: {
    close: '35.41200000',
    open: '35.22800000',
    high: '39.14200000',
    low: '33.47000000',
    volume: '6717425.63000000',
    quoteVolume: '242384607.53676800',
    eventTime: 1620227052705
  },
  BTTUSDT: {
    close: '0.00705960',
    open: '0.00643330',
    high: '0.00744000',
    low: '0.00621050',
    volume: '62163193430.00000000',
    quoteVolume: '424639378.34384190',
    eventTime: 1620227053144
  },
  ONGUSDT: {
    close: '0.95780000',
    open: '0.89510000',
    high: '0.99310000',
    low: '0.85770000',
    volume: '9420254.58000000',
    quoteVolume: '8735629.35782400',
    eventTime: 1620227052309
  },
  HOTUSDT: {
    close: '0.01512100',
    open: '0.01378300',
    high: '0.01619600',
    low: '0.01311500',
    volume: '13249273190.00000000',
    quoteVolume: '193968637.69711700',
    eventTime: 1620227052975
  },
  ZILUSDT: {
    close: '0.21088000',
    open: '0.20220000',
    high: '0.21337000',
    low: '0.19073000',
    volume: '296495620.80000000',
    quoteVolume: '59771931.62427700',
    eventTime: 1620227052501
  },
  FETUSDT: {
    close: '0.59670000',
    open: '0.56987000',
    high: '0.60500000',
    low: '0.53089000',
    volume: '54764429.00000000',
    quoteVolume: '31069106.62603700',
    eventTime: 1620227052642
  },
  BATUSDT: {
    close: '1.30100000',
    open: '1.18730000',
    high: '1.30310000',
    low: '1.13110000',
    volume: '25887038.87000000',
    quoteVolume: '31449461.60900000',
    eventTime: 1620227053220
  },
  ZECUSDT: {
    close: '267.30000000',
    open: '251.86000000',
    high: '273.27000000',
    low: '232.33000000',
    volume: '541438.55578000',
    quoteVolume: '138426900.23631520',
    eventTime: 1620227052880
  },
  IOSTUSDT: {
    close: '0.06125700',
    open: '0.05836200',
    high: '0.06288100',
    low: '0.05515400',
    volume: '955812347.00000000',
    quoteVolume: '56049555.42555300',
    eventTime: 1620227053114
  },
  NANOUSDT: {
    close: '10.15030000',
    open: '9.05010000',
    high: '10.49500000',
    low: '8.62220000',
    volume: '3849260.71000000',
    quoteVolume: '36501858.96454000',
    eventTime: 1620227053184
  },
  OMGUSDT: {
    close: '9.96190000',
    open: '9.26310000',
    high: '10.18470000',
    low: '8.54270000',
    volume: '14492310.34000000',
    quoteVolume: '135835798.67247200',
    eventTime: 1620227053186
  },
  THETAUSDT: {
    close: '11.02300000',
    open: '10.57200000',
    high: '11.10000000',
    low: '10.01300000',
    volume: '18185742.70900000',
    quoteVolume: '193065591.24423600',
    eventTime: 1620227052458
  },
  MATICUSDT: {
    close: '0.79097000',
    open: '0.73005000',
    high: '0.83000000',
    low: '0.68720000',
    volume: '330357185.80000000',
    quoteVolume: '247221968.09826700',
    eventTime: 1620227052603
  },
  TFUELUSDT: {
    close: '0.37841000',
    open: '0.33677000',
    high: '0.42400000',
    low: '0.31903000',
    volume: '172979559.40000000',
    quoteVolume: '64207526.21622100',
    eventTime: 1620227052507
  },
  FTMUSDT: {
    close: '0.81480000',
    open: '0.68890000',
    high: '0.83851000',
    low: '0.63100000',
    volume: '293895519.10000000',
    quoteVolume: '219327139.69150300',
    eventTime: 1620227053037
  },
  GTOUSDT: {
    close: '0.08474000',
    open: '0.08003000',
    high: '0.08520000',
    low: '0.07558000',
    volume: '113218806.80000000',
    quoteVolume: '9074566.12856800',
    eventTime: 1620227052421
  },
  DOGEBTC: {
    close: '0.00001128',
    open: '0.00000967',
    high: '0.00001275',
    low: '0.00000917',
    volume: '3607879059.00000000',
    quoteVolume: '40110.33204166',
    eventTime: 1620227053191
  },
  DOGEUSDT: {
    close: '0.64582000',
    open: '0.52998000',
    high: '0.69663000',
    low: '0.49592000',
    volume: '18413068513.80000000',
    quoteVolume: '11202291539.25210550',
    eventTime: 1620227053295
  },
  DUSKUSDT: {
    close: '0.27700000',
    open: '0.25860000',
    high: '0.27950000',
    low: '0.24790000',
    volume: '14732429.96000000',
    quoteVolume: '3884901.95958600',
    eventTime: 1620227052434
  },
  ANKRUSDT: {
    close: '0.16390000',
    open: '0.15680000',
    high: '0.16629000',
    low: '0.15100000',
    volume: '167556532.60000000',
    quoteVolume: '26469143.24619900',
    eventTime: 1620227053244
  },
  WINUSDT: {
    close: '0.00125800',
    open: '0.00115950',
    high: '0.00130920',
    low: '0.00112000',
    volume: '167841353337.00000000',
    quoteVolume: '203828175.99480340',
    eventTime: 1620227053113
  },
  DENTUSDT: {
    close: '0.01021000',
    open: '0.00957750',
    high: '0.01046080',
    low: '0.00905500',
    volume: '14599402286.00000000',
    quoteVolume: '142574080.78476830',
    eventTime: 1620227052838
  },
  FUNUSDT: {
    close: '0.04209100',
    open: '0.03938900',
    high: '0.04219900',
    low: '0.03766900',
    volume: '522185250.00000000',
    quoteVolume: '20523953.26486900',
    eventTime: 1620227053186
  },
  CHZUSDT: {
    close: '0.51914000',
    open: '0.50120000',
    high: '0.52500000',
    low: '0.47244000',
    volume: '619986239.00000000',
    quoteVolume: '311399479.07095900',
    eventTime: 1620227052945
  },
  BANDUSDT: {
    close: '19.38300000',
    open: '16.80800000',
    high: '20.67300000',
    low: '15.78400000',
    volume: '6585514.64100000',
    quoteVolume: '125559184.19366600',
    eventTime: 1620227052677
  },
  BNBBUSD: {
    close: '650.56000000',
    open: '624.60000000',
    high: '657.00000000',
    low: '605.11000000',
    volume: '691098.15770000',
    quoteVolume: '435297824.45003400',
    eventTime: 1620227053091
  },
  BTCBUSD: {
    close: '57263.97000000',
    open: '54872.79000000',
    high: '57381.81000000',
    low: '52937.60000000',
    volume: '19024.26688800',
    quoteVolume: '1042308500.33310916',
    eventTime: 1620227052826
  },
  BUSDUSDT: {
    close: '0.99900000',
    open: '0.99940000',
    high: '1.00000000',
    low: '0.99870000',
    volume: '1907052499.83000000',
    quoteVolume: '1905771673.78656800',
    eventTime: 1620227053192
  },
  XTZUSDT: {
    close: '6.42760000',
    open: '5.79110000',
    high: '6.49900000',
    low: '5.40120000',
    volume: '17271537.37000000',
    quoteVolume: '103825282.46607800',
    eventTime: 1620227053266
  },
  NKNUSDT: {
    close: '0.69633000',
    open: '0.68985000',
    high: '0.71000000',
    low: '0.65000000',
    volume: '51086369.70000000',
    quoteVolume: '34887916.03693700',
    eventTime: 1620227052326
  },
  ETHBUSD: {
    close: '3351.96000000',
    open: '3381.97000000',
    high: '3431.31000000',
    low: '3162.46000000',
    volume: '521935.59832000',
    quoteVolume: '1731528909.67163310',
    eventTime: 1620227052941
  },
  KAVAUSDT: {
    close: '6.17910000',
    open: '5.84570000',
    high: '6.28080000',
    low: '5.56470000',
    volume: '4536808.35000000',
    quoteVolume: '27036710.13977700',
    eventTime: 1620227052679
  },
  IOTXUSDT: {
    close: '0.05793300',
    open: '0.05541400',
    high: '0.05890700',
    low: '0.05320900',
    volume: '417325664.00000000',
    quoteVolume: '23413425.78427200',
    eventTime: 1620227052461
  },
  ADABUSD: {
    close: '1.39880000',
    open: '1.29880000',
    high: '1.40000000',
    low: '1.25500000',
    volume: '57774547.70000000',
    quoteVolume: '76384806.06314800',
    eventTime: 1620227052867
  },
  CTXCUSDT: {
    close: '0.40760000',
    open: '0.36780000',
    high: '0.42510000',
    low: '0.34730000',
    volume: '13057243.14000000',
    quoteVolume: '5048464.79893400',
    eventTime: 1620227052315
  },
  BCHBTC: {
    close: '0.02175700',
    open: '0.01814000',
    high: '0.02249900',
    low: '0.01748000',
    volume: '118684.28700000',
    quoteVolume: '2393.31634688',
    eventTime: 1620227053143
  },
  BCHUSDT: {
    close: '1244.35000000',
    open: '994.49000000',
    high: '1250.00000000',
    low: '930.69000000',
    volume: '614335.35896000',
    quoteVolume: '674710656.42970950',
    eventTime: 1620227053297
  },
  VITEUSDT: {
    close: '0.20151000',
    open: '0.19557000',
    high: '0.20800000',
    low: '0.17646000',
    volume: '87116287.80000000',
    quoteVolume: '16497210.70551100',
    eventTime: 1620227053222
  },
  USDTTRY: {
    close: '8.36200000',
    open: '8.37100000',
    high: '8.41500000',
    low: '8.25000000',
    volume: '209875121.56000000',
    quoteVolume: '1755153470.68200000',
    eventTime: 1620227052645
  },
  BTCEUR: {
    close: '47752.96000000',
    open: '45614.59000000',
    high: '47831.12000000',
    low: '44157.15000000',
    volume: '4047.14993700',
    quoteVolume: '184561386.87488178',
    eventTime: 1620227053124
  },
  EURUSDT: {
    close: '1.19820000',
    open: '1.20200000',
    high: '1.20560000',
    low: '1.19570000',
    volume: '233963825.19000000',
    quoteVolume: '280450079.58762900',
    eventTime: 1620227052952
  },
  WRXBTC: {
    close: '0.00004392',
    open: '0.00004249',
    high: '0.00004650',
    low: '0.00004180',
    volume: '7948452.00000000',
    quoteVolume: '347.04876343',
    eventTime: 1620227052316
  },
  WRXUSDT: {
    close: '2.51370000',
    open: '2.32970000',
    high: '2.60220000',
    low: '2.24000000',
    volume: '23004107.03000000',
    quoteVolume: '55091087.55567700',
    eventTime: 1620227053265
  },
  LSKUSDT: {
    close: '5.78000000',
    open: '5.71400000',
    high: '5.79590000',
    low: '5.22890000',
    volume: '2401329.83000000',
    quoteVolume: '13285895.15640800',
    eventTime: 1620227052833
  },
  BATBUSD: {
    close: '1.30080000',
    open: '1.18960000',
    high: '1.30360000',
    low: '1.13800000',
    volume: '2769742.57000000',
    quoteVolume: '3373782.46587700',
    eventTime: 1620227053215
  },
  NANOBUSD: {
    close: '10.14930000',
    open: '9.08210000',
    high: '10.48920000',
    low: '8.61510000',
    volume: '461170.11000000',
    quoteVolume: '4371164.31182200',
    eventTime: 1620227053155
  },
  ONTBUSD: {
    close: '1.99630000',
    open: '1.82020000',
    high: '2.00800000',
    low: '1.72240000',
    volume: '3637150.76000000',
    quoteVolume: '6818134.04212800',
    eventTime: 1620227053269
  },
  HIVEBTC: {
    close: '0.00001044',
    open: '0.00001084',
    high: '0.00001128',
    low: '0.00001023',
    volume: '3129009.00000000',
    quoteVolume: '33.36640606',
    eventTime: 1620227052984
  },
  HIVEUSDT: {
    close: '0.59600000',
    open: '0.59350000',
    high: '0.61020000',
    low: '0.55500000',
    volume: '10707909.11000000',
    quoteVolume: '6216032.89606900',
    eventTime: 1620227052974
  },
  BTCDOWNUSDT: {
    close: '0.04255000',
    open: '0.04686000',
    high: '0.05078000',
    low: '0.04201000',
    volume: '629735493.36000000',
    quoteVolume: '29760510.87787910',
    eventTime: 1620227053152
  },
  ARDRUSDT: {
    close: '0.36574000',
    open: '0.35560000',
    high: '0.37186000',
    low: '0.33700000',
    volume: '10837783.60000000',
    quoteVolume: '3848606.86333300',
    eventTime: 1620227052941
  },
  WRXBUSD: {
    close: '2.51510000',
    open: '2.32780000',
    high: '2.59990000',
    low: '2.22680000',
    volume: '1574906.37000000',
    quoteVolume: '3795931.79215800',
    eventTime: 1620227052583
  },
  KNCBUSD: {
    close: '3.22100000',
    open: '3.11400000',
    high: '3.22700000',
    low: '2.93600000',
    volume: '315039.82100000',
    quoteVolume: '976826.24823300',
    eventTime: 1620227052413
  },
  KNCUSDT: {
    close: '3.21400000',
    open: '3.10800000',
    high: '3.22400000',
    low: '2.91400000',
    volume: '3580821.18900000',
    quoteVolume: '11054705.69085300',
    eventTime: 1620227052425
  },
  IQBUSD: {
    close: '0.02140100',
    open: '0.02158900',
    high: '0.02227400',
    low: '0.01963600',
    volume: '162000428.00000000',
    quoteVolume: '3392665.40197200',
    eventTime: 1620227053177
  },
  PNTBTC: {
    close: '0.00005212',
    open: '0.00003171',
    high: '0.00006722',
    low: '0.00003080',
    volume: '10205906.00000000',
    quoteVolume: '518.41404093',
    eventTime: 1620227052753
  },
  PNTUSDT: {
    close: '2.98050000',
    open: '1.74410000',
    high: '3.72270000',
    low: '1.65720000',
    volume: '69312686.51000000',
    quoteVolume: '197634750.04362200',
    eventTime: 1620227053100
  },
  XRPGBP: {
    close: '1.13440000',
    open: '1.01524000',
    high: '1.15892000',
    low: '0.95453000',
    volume: '30443536.00000000',
    quoteVolume: '31798945.59655500',
    eventTime: 1620227052943
  },
  SNXBTC: {
    close: '0.00030740',
    open: '0.00031290',
    high: '0.00031800',
    low: '0.00030110',
    volume: '520588.69000000',
    quoteVolume: '161.24161638',
    eventTime: 1620227052569
  },
  SNXUSDT: {
    close: '17.59400000',
    open: '17.15500000',
    high: '17.74000000',
    low: '16.19400000',
    volume: '1246676.64900000',
    quoteVolume: '21097669.80624000',
    eventTime: 1620227052580
  },
  ADAUPUSDT: {
    close: '87.83200000',
    open: '76.59200000',
    high: '88.39800000',
    low: '71.33300000',
    volume: '205853.49000000',
    quoteVolume: '16577608.38553000',
    eventTime: 1620227053304
  },
  SXPUSDT: {
    close: '5.11900000',
    open: '4.98200000',
    high: '5.20000000',
    low: '4.56000000',
    volume: '78125756.50600000',
    quoteVolume: '386160426.45521100',
    eventTime: 1620227052718
  },
  MKRUSDT: {
    close: '5382.93000000',
    open: '5459.49000000',
    high: '5586.94000000',
    low: '5093.05000000',
    volume: '14489.51081000',
    quoteVolume: '77283507.18952090',
    eventTime: 1620227052943
  },
  DOGEBUSD: {
    close: '0.64630000',
    open: '0.53031000',
    high: '0.69920000',
    low: '0.49740000',
    volume: '1629508011.50000000',
    quoteVolume: '990242718.00515400',
    eventTime: 1620227053308
  },
  BTCAUD: {
    close: '74205.04000000',
    open: '71530.65000000',
    high: '74400.00000000',
    low: '69013.45000000',
    volume: '520.13457800',
    quoteVolume: '36990885.01078296',
    eventTime: 1620227053203
  },
  FIOBTC: {
    close: '0.00000604',
    open: '0.00000604',
    high: '0.00000621',
    low: '0.00000583',
    volume: '4047201.00000000',
    quoteVolume: '24.30031422',
    eventTime: 1620227052763
  },
  FIOBUSD: {
    close: '0.34570000',
    open: '0.33010000',
    high: '0.34570000',
    low: '0.31520000',
    volume: '2253261.41000000',
    quoteVolume: '743049.37904000',
    eventTime: 1620227052743
  },
  AUDUSDT: {
    close: '0.77066000',
    open: '0.76588000',
    high: '0.78229000',
    low: '0.76416000',
    volume: '51046735.50000000',
    quoteVolume: '39209827.93059100',
    eventTime: 1620227053298
  },
  DOTBNB: {
    close: '0.06037000',
    open: '0.05797000',
    high: '0.06140000',
    low: '0.05647000',
    volume: '735516.80000000',
    quoteVolume: '42736.09182400',
    eventTime: 1620227052947
  },
  DOTBUSD: {
    close: '39.29200000',
    open: '36.25100000',
    high: '39.78980000',
    low: '34.50000000',
    volume: '1722977.80200000',
    quoteVolume: '63425750.39660300',
    eventTime: 1620227052862
  },
  DOTUSDT: {
    close: '39.23200000',
    open: '36.20500000',
    high: '39.76300000',
    low: '34.36600000',
    volume: '15390625.79200000',
    quoteVolume: '565772580.96274900',
    eventTime: 1620227052515
  },
  BZRXUSDT: {
    close: '1.04250000',
    open: '0.86900000',
    high: '1.17580000',
    low: '0.80080000',
    volume: '109850137.48000000',
    quoteVolume: '110140771.62827400',
    eventTime: 1620227052943
  },
  EGLDBNB: {
    close: '0.29770000',
    open: '0.29080000',
    high: '0.30690000',
    low: '0.28590000',
    volume: '9105.38000000',
    quoteVolume: '2680.98469300',
    eventTime: 1620227053121
  },
  EGLDBTC: {
    close: '0.00338100',
    open: '0.00329900',
    high: '0.00354000',
    low: '0.00324400',
    volume: '44028.02800000',
    quoteVolume: '149.51283351',
    eventTime: 1620227053131
  },
  DIAUSDT: {
    close: '5.48100000',
    open: '4.48000000',
    high: '5.80000000',
    low: '4.15000000',
    volume: '14431407.80300000',
    quoteVolume: '75238264.79579100',
    eventTime: 1620227053220
  },
  FIOUSDT: {
    close: '0.34490000',
    open: '0.33080000',
    high: '0.34570000',
    low: '0.31430000',
    volume: '14609817.80000000',
    quoteVolume: '4816247.81624700',
    eventTime: 1620227052776
  },
  CREAMBNB: {
    close: '0.21840000',
    open: '0.23330000',
    high: '0.23580000',
    low: '0.20870000',
    volume: '12265.05000000',
    quoteVolume: '2695.86335100',
    eventTime: 1620227052947
  },
  CREAMBUSD: {
    close: '142.90000000',
    open: '146.19000000',
    high: '152.36000000',
    low: '132.41000000',
    volume: '39570.87800000',
    quoteVolume: '5528837.57228500',
    eventTime: 1620227052923
  },
  UNIBTC: {
    close: '0.00076310',
    open: '0.00077039',
    high: '0.00083000',
    low: '0.00074772',
    volume: '1215469.50000000',
    quoteVolume: '958.09014043',
    eventTime: 1620227052653
  },
  UNIUSDT: {
    close: '43.67000000',
    open: '42.24200000',
    high: '44.90000000',
    low: '40.00000000',
    volume: '7196413.07200000',
    quoteVolume: '309841195.42292100',
    eventTime: 1620227053127
  },
  AVAXUSDT: {
    close: '33.04700000',
    open: '31.93300000',
    high: '33.35800000',
    low: '29.50100000',
    volume: '3397248.11300000',
    quoteVolume: '106859998.34273200',
    eventTime: 1620227053246
  },
  BURGERBNB: {
    close: '0.02574000',
    open: '0.02843000',
    high: '0.03130000',
    low: '0.02472000',
    volume: '1070275.80000000',
    quoteVolume: '29445.40222300',
    eventTime: 1620227052563
  },
  UTKBTC: {
    close: '0.00001657',
    open: '0.00001301',
    high: '0.00001910',
    low: '0.00001212',
    volume: '30841946.00000000',
    quoteVolume: '481.47451825',
    eventTime: 1620227052883
  },
  UTKUSDT: {
    close: '0.94810000',
    open: '0.71310000',
    high: '1.06500000',
    low: '0.65120000',
    volume: '169053355.85000000',
    quoteVolume: '149552351.85625700',
    eventTime: 1620227052891
  },
  XVSUSDT: {
    close: '120.00000000',
    open: '115.29900000',
    high: '124.37500000',
    low: '108.24800000',
    volume: '736354.07000000',
    quoteVolume: '85694244.61205600',
    eventTime: 1620227053174
  },
  BTCBRL: {
    close: '311298.00000000',
    open: '302901.00000000',
    high: '311299.00000000',
    low: '292504.00000000',
    volume: '519.24260900',
    quoteVolume: '155983354.87995300',
    eventTime: 1620227053301
  },
  USDTBRL: {
    close: '5.44000000',
    open: '5.52600000',
    high: '5.54500000',
    low: '5.42200000',
    volume: '25506735.72000000',
    quoteVolume: '140227633.99228000',
    eventTime: 1620227053286
  },
  NEARUSDT: {
    close: '5.13770000',
    open: '4.97620000',
    high: '5.18330000',
    low: '4.76410000',
    volume: '4757485.90000000',
    quoteVolume: '23618075.59958000',
    eventTime: 1620227052462
  },
  FILBTC: {
    close: '0.00266290',
    open: '0.00272710',
    high: '0.00280650',
    low: '0.00251480',
    volume: '151833.17000000',
    quoteVolume: '406.66939315',
    eventTime: 1620227052460
  },
  FILUSDT: {
    close: '152.37000000',
    open: '149.45000000',
    high: '154.86000000',
    low: '135.10000000',
    volume: '1398955.65140000',
    quoteVolume: '204689938.95810300',
    eventTime: 1620227052472
  },
  FILUPUSDT: {
    close: '17.80000000',
    open: '17.15500000',
    high: '18.39400000',
    low: '13.76200000',
    volume: '499461.15000000',
    quoteVolume: '7799580.94186000',
    eventTime: 1620227052786
  },
  AERGOBTC: {
    close: '0.00000537',
    open: '0.00000557',
    high: '0.00000581',
    low: '0.00000525',
    volume: '2879101.00000000',
    quoteVolume: '15.91998365',
    eventTime: 1620227052943
  },
  AERGOBUSD: {
    close: '0.30626000',
    open: '0.30559000',
    high: '0.31642000',
    low: '0.28469000',
    volume: '2361548.10000000',
    quoteVolume: '710391.24590300',
    eventTime: 1620227052938
  },
  DOTEUR: {
    close: '32.80800000',
    open: '30.10100000',
    high: '33.20000000',
    low: '28.76300000',
    volume: '393886.45700000',
    quoteVolume: '12102393.52863200',
    eventTime: 1620227052852
  },
  SLPETH: {
    close: '0.00009058',
    open: '0.00008161',
    high: '0.00009915',
    low: '0.00007663',
    volume: '41995222.00000000',
    quoteVolume: '3695.60749151',
    eventTime: 1620227053057
  },
  ADAEUR: {
    close: '1.16620000',
    open: '1.07960000',
    high: '1.16830000',
    low: '1.04790000',
    volume: '24268909.61000000',
    quoteVolume: '26956421.53632500',
    eventTime: 1620227052842
  },
  STRAXUSDT: {
    close: '3.23390000',
    open: '2.54120000',
    high: '3.55560000',
    low: '2.42860000',
    volume: '20841169.87000000',
    quoteVolume: '63598771.46767500',
    eventTime: 1620227052670
  },
  UNFIBTC: {
    close: '0.00047080',
    open: '0.00046664',
    high: '0.00051800',
    low: '0.00045725',
    volume: '134128.90000000',
    quoteVolume: '64.38447639',
    eventTime: 1620227052902
  },
  FRONTBUSD: {
    close: '2.69860000',
    open: '2.60830000',
    high: '2.74530000',
    low: '2.50750000',
    volume: '2917571.25000000',
    quoteVolume: '7675050.68404200',
    eventTime: 1620227052744
  },
  SYSBUSD: {
    close: '0.81399000',
    open: '0.57500000',
    high: '0.90000000',
    low: '0.54176000',
    volume: '19757801.00000000',
    quoteVolume: '14407536.94347800',
    eventTime: 1620227053303
  },
  GRTBTC: {
    close: '0.00002673',
    open: '0.00002721',
    high: '0.00002800',
    low: '0.00002655',
    volume: '3464355.00000000',
    quoteVolume: '94.51484171',
    eventTime: 1620227052347
  },
  PSGUSDT: {
    close: '35.35000000',
    open: '36.81000000',
    high: '40.49500000',
    low: '32.07100000',
    volume: '1232957.32100000',
    quoteVolume: '43200456.87197400',
    eventTime: 1620227052880
  },
  '1INCHUSDT': {
    close: '5.78800000',
    open: '5.50860000',
    high: '5.86990000',
    low: '5.18550000',
    volume: '10967818.30000000',
    quoteVolume: '60625879.24958300',
    eventTime: 1620227053286
  },
  CELOUSDT: {
    close: '5.27710000',
    open: '5.10000000',
    high: '5.36000000',
    low: '4.95960000',
    volume: '1917878.29000000',
    quoteVolume: '9855235.06724200',
    eventTime: 1620227052897
  },
  BTCSTUSDT: {
    close: '74.57000000',
    open: '65.19100000',
    high: '75.00000000',
    low: '62.00000000',
    volume: '276688.75600000',
    quoteVolume: '18657695.98300400',
    eventTime: 1620227053233
  },
  USDCBUSD: {
    close: '1.00000000',
    open: '1.00000000',
    high: '1.00020000',
    low: '1.00000000',
    volume: '94036142.31000000',
    quoteVolume: '94041828.86965300',
    eventTime: 1620227052781
  },
  TWTUSDT: {
    close: '1.03860000',
    open: '0.97200000',
    high: '1.09350000',
    low: '0.93230000',
    volume: '37524162.98000000',
    quoteVolume: '37798762.69492300',
    eventTime: 1620227053245
  },
  DOGEEUR: {
    close: '0.53903000',
    open: '0.44094000',
    high: '0.58150000',
    low: '0.41327000',
    volume: '791658192.10000000',
    quoteVolume: '402989397.44075900',
    eventTime: 1620227053024
  },
  DOGETRY: {
    close: '5.40210000',
    open: '4.43660000',
    high: '5.80450000',
    low: '4.15000000',
    volume: '1140106468.00000000',
    quoteVolume: '5830776987.24210000',
    eventTime: 1620227052602
  },
  DOGEBRL: {
    close: '3.51410000',
    open: '2.92990000',
    high: '3.81940000',
    low: '2.73280000',
    volume: '83672798.62000000',
    quoteVolume: '277769907.95856300',
    eventTime: 1620227053284
  },
  PROSETH: {
    close: '0.00164300',
    open: '0.00157300',
    high: '0.00197000',
    low: '0.00131500',
    volume: '1607483.72000000',
    quoteVolume: '2600.03792836',
    eventTime: 1620227052345
  },
  DOGEGBP: {
    close: '0.46659000',
    open: '0.38262000',
    high: '0.50171000',
    low: '0.35900000',
    volume: '334797171.70000000',
    quoteVolume: '146929240.29895600',
    eventTime: 1620227053201
  },
  FISBTC: {
    close: '0.00005293',
    open: '0.00005156',
    high: '0.00005602',
    low: '0.00004898',
    volume: '437230.50000000',
    quoteVolume: '22.99862761',
    eventTime: 1620227052437
  },
  FISUSDT: {
    close: '3.03000000',
    open: '2.82900000',
    high: '3.10000000',
    low: '2.66900000',
    volume: '2367223.26700000',
    quoteVolume: '6830794.44897600',
    eventTime: 1620227052428
  },
  PONDBTC: {
    close: '0.00000324',
    open: '0.00000333',
    high: '0.00000342',
    low: '0.00000320',
    volume: '8233560.00000000',
    quoteVolume: '27.19207803',
    eventTime: 1620227052796
  },
  PONDBUSD: {
    close: '0.18530000',
    open: '0.18320000',
    high: '0.18890000',
    low: '0.17090000',
    volume: '4676951.33000000',
    quoteVolume: '845069.95588900',
    eventTime: 1620227052792
  },
  PONDUSDT: {
    close: '0.18450000',
    open: '0.18260000',
    high: '0.18930000',
    low: '0.17020000',
    volume: '56054380.96000000',
    quoteVolume: '10140984.24660700',
    eventTime: 1620227052785
  },
  LINAUSDT: {
    close: '0.11330000',
    open: '0.11230000',
    high: '0.11460000',
    low: '0.10200000',
    volume: '487845844.18000000',
    quoteVolume: '53223495.20534700',
    eventTime: 1620227053241
  },
  SUPERBTC: {
    close: '0.00004312',
    open: '0.00004181',
    high: '0.00004681',
    low: '0.00004095',
    volume: '1220210.20000000',
    quoteVolume: '53.78800438',
    eventTime: 1620227052787
  },
  CFXUSDT: {
    close: '0.91000000',
    open: '0.89100000',
    high: '0.92400000',
    low: '0.85000000',
    volume: '4843360.03000000',
    quoteVolume: '4278637.44018000',
    eventTime: 1620227053096
  },
  EPSBTC: {
    close: '0.00004925',
    open: '0.00004821',
    high: '0.00005331',
    low: '0.00004591',
    volume: '2669925.80000000',
    quoteVolume: '129.84249924',
    eventTime: 1620227053270
  },
  EPSUSDT: {
    close: '2.81700000',
    open: '2.65500000',
    high: '2.90100000',
    low: '2.45000000',
    volume: '18503710.88600000',
    quoteVolume: '49232601.05061400',
    eventTime: 1620227053234
  },
  WINEUR: {
    close: '0.00104800',
    open: '0.00096600',
    high: '0.00109600',
    low: '0.00089400',
    volume: '2402039061.00000000',
    quoteVolume: '2412153.93185600',
    eventTime: 1620227052461
  },
  TLMBTC: {
    close: '0.00001155',
    open: '0.00001138',
    high: '0.00001322',
    low: '0.00001070',
    volume: '35433326.50000000',
    quoteVolume: '418.30869762',
    eventTime: 1620227052507
  },
  TLMBUSD: {
    close: '0.66130000',
    open: '0.62490000',
    high: '0.73000000',
    low: '0.57540000',
    volume: '40666456.46000000',
    quoteVolume: '26315525.27822100',
    eventTime: 1620227053117
  },
  TLMUSDT: {
    close: '0.66010000',
    open: '0.62250000',
    high: '0.72590000',
    low: '0.57620000',
    volume: '335562187.42000000',
    quoteVolume: '216883727.44693500',
    eventTime: 1620227052513
  },
  BTGUSDT: {
    close: '118.16500000',
    open: '101.25000000',
    high: '119.71700000',
    low: '96.00000000',
    volume: '274382.05000000',
    quoteVolume: '29859132.37825000',
    eventTime: 1620227052862
  },
  BAKEUSDT: {
    close: '6.45820000',
    open: '6.23470000',
    high: '6.80000000',
    low: '5.77880000',
    volume: '20022543.70000000',
    quoteVolume: '125832439.73654000',
    eventTime: 1620227052988
  },
  SLPUSDT: {
    close: '0.30330000',
    open: '0.27500000',
    high: '0.33430000',
    low: '0.25550000',
    volume: '438287378.00000000',
    quoteVolume: '129408043.59740000',
    eventTime: 1620227053018
  }
}
