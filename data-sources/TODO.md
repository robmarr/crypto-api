# TODO

## MVP

1. ~~Create code to collect from [binance websockets](https://www.npmjs.com/package/binance)~~
2. ~~Create code to collect from [Uphold API](https://uphold.com/en/developer/api/documentation/https://uphold.com/en/developer/api/documentation/)~~
3. Create code to collect from [coingecko websocket/api](https://www.coingecko.com/api/documentations/v3#/)
4. Store data in a git repo using [nodegit](https://www.nodegit.org/) 

## Stretch

1. Create code to collect from [Bitfinrex websockets](https://bitfinex.readthedocs.io/en/latest/websocket.html)
2. ~~Create code to get data from [bittrex api/websockets](https://bittrex.github.io/api/v3)~~
