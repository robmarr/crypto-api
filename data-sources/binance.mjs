import EventEmitter from 'events'
import BinanceAPi from '../lib/binance-api.mjs'
export class BinancePure extends EventEmitter {
  constructor (opts = {}) {
    super()
    this.openSockets = []

    if (opts.cacheKey && typeof opts.cacheKey !== 'string') throw TypeError('cacheKey must be a string')
    this.cacheKey = opts.cacheKey || 'binance'
  }

  checkMiniTickerResponse (data) {
    const [symbol, { close }] = Object.entries(data)[0]
    if (symbol.length >= 6 && isFinite(parseFloat(close))) return true
    throw new Error('Unexpected data format')
  }

  parseMiniTicker (data) {
    return Object.entries(data)
      .reduce((prices, [symbol, { close }]) => {
        prices[symbol] = parseFloat(close)
        return prices
      }, {})
  }

  startMiniTicker () {
    return this.dataSource.miniTicker((data) => {
      try {
        this.checkMiniTickerResponse(data)
        const ticker = this.parseMiniTicker(data)
        this.emit('data', { cacheKey: this.cacheKey, data: ticker })
      } catch (error) {
        this.emit('error', new Error(`failed to parse data because ${error.message}`))
      }
    })
  }

  start () {
    this.openSockets = [...this.openSockets, this.startMiniTicker()]
  }

  stop () {
    this.openSockets.forEach(socket => this.dataSource.terminate(socket))
    this.openSockets = []
  }
}

export default BinanceAPi(BinancePure)
