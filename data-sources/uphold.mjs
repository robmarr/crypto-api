import EventEmitter from 'events'
import UpholdAPI from '../lib/uphold-api.mjs'

export class UpholdPure extends EventEmitter {
  constructor (opts = {}) {
    super()
    this.timer = null

    if (opts.cacheKey && typeof opts.cacheKey !== 'string') throw TypeError('cacheKey must be a string')
    this.cacheKey = opts.cacheKey || 'uphold'
    if (opts.tickers && !Array.isArray(opts.tickers)) throw TypeError('tickers must be an array')
    this.tickers = opts.tickers || ['BTC', 'USD', 'GBP', 'ETH']
    this.interval = this.constructor.intervalToMilliseconds(parseFloat(opts.interval || 10))
  }

  static intervalToMilliseconds (minutes) {
    if (!(typeof minutes === 'number' && isFinite(minutes))) throw TypeError('interval minutes must be a number')
    return minutes * 60 * 1000
  }

  static parseTickerData (data) {
    return data.filter(({ pair }) => !pair.includes('-')).reduce((prices, { pair, ask }) => ({ ...prices, [pair]: parseFloat(ask) }), {})
  }

  runTickers () {
    return Promise.all(this.tickers.map((symbol) => this.fetchTicker(symbol)))
      .then(data => (
        this.constructor.parseTickerData(data.flat())
      ))
      .then(data => (
        this.emit('data', { cacheKey: this.cacheKey, data })
      ))
      .catch((error) => this.emit('error', new Error(error.message)))
  }

  start () {
    this.runTickers()
    this.timer = setInterval(this.runTickers.bind(this), this.interval)
  }

  stop () {
    clearInterval(this.timer)
    this.timers = null
  }
}

export default UpholdAPI(UpholdPure)
