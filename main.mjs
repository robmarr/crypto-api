
import fs from 'fs'
import { join, resolve } from 'path'
import EventEmitter from 'events'
import chokidar from 'chokidar'

import API from './api.mjs'
import DataCollector from './data-collector.mjs'
import CacheMixin from './lib/cache.mjs'
import AppMixin from './lib/app.mjs'

class Config extends EventEmitter {
  constructor (opts = {}) {
    super()
    this._watch = chokidar.watch
    this.file = join(process.cwd(), 'config.json')
    this.watcher = null
    this.watcherOptions = {
      persistent: true
    }
  }

  read () {
    return fs.promises.readFile(resolve(this.file))
      .then(content => JSON.parse(content))
  }

  watch () {
    this.watcher = this._watch(this.file, this.watcherOptions)
    this.watcher.on('change', async () => {
      this.emit('update')
    })
  }

  start () {
    this.watch()
    return this.read()
  }

  stop () {
    return this.watcher.close()
  }
}

class Main extends CacheMixin(AppMixin(Object)) {
  constructor () {
    super()
    this.config = new Config()
    this.api = new API({ server: this.app, cache: this.cache })
    this.collector = new DataCollector({ cache: this.cache })
  }

  reload () {
    this.server.on('close', () => {
      this.start()
      this.server.off('close', this.reload)
    })
    this.server.close()
  }

  async start () {
    const opts = await this.config.start()
    this.config.on('update', () => this.reload())
    this.collector.start()
    this.api.start()
    this.server = this.app.listen(opts.port, () => {
      console.log(`server listening on ${opts.port}`)
    })
  }
}

const app = new Main()

console.log(app)
app.start()
