import Binance from './data-sources/binance.mjs'
import Uphold from './data-sources/uphold.mjs'
import Bittrex from './data-sources/bittrex.mjs'

export default class DataCollector {
  constructor (opts = { }) {
    this.cache = opts.cache
    this.sources = [new Binance(), new Uphold(), new Bittrex()]
  }

  onData ({ cacheKey, data }) {
    console.log(`caching data from ${cacheKey}`)
    const current = this.cache.get(cacheKey) ?? {}
    this.cache.set(cacheKey, Object.assign(current, data))
  }

  start () {
    this.sources.forEach(source => {
      source.on('data', this.onData.bind(this))
      source.start()
    })
  }

  stop () {
    this.sources.forEach(source => {
      source.stop()
    })
  }
}
